<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

$id = $_GET['id'];

$items = [
    1 => 'https://images.pexels.com/photos/46710/pexels-photo-46710.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=300&w=500',
    2 => 'https://images.pexels.com/photos/449627/pexels-photo-449627.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=300&w=500',
    3 => 'https://images.pexels.com/photos/1118448/pexels-photo-1118448.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=300&w=500',
    4 => 'https://images.pexels.com/photos/871060/pexels-photo-871060.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=300&w=500',
    5 => 'https://images.pexels.com/photos/767239/pexels-photo-767239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=300&w=500'
];

$ret = [
        "id" => $id,
        "type" => "image",
        "src" => $items[$id],
        "title" => "..."
];

echo json_encode($ret);
