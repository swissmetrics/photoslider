export interface PhotosModel {
    jednostka_id: number,
    jednostka_nazwa: string,
    elementy: PhotoModel[],
    objekt_id: number
}

export interface CollectionModel {
    kolekcja_id: number,
    kolekcja_nazwa: string,
    elementy: PhotoModel[]
}

export interface PhotoModel {
    id: number,
    typ: 'image' | 'video' | 'audio',
    src: string,
    nazwa: string,
    autor: string,
    daty: string,
    opis: string,
    prawa: string,
    objekt_id: number
}

export interface FoldersModel {
    id: number,
    name: string,
    children?: {
        id: number,
        name: string
    }[]
}
