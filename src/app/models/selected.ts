export interface PinModel {
    id: number,
    idPliku: number,
    left: number,
    opis: string,
    top: number
}

export interface CutModel {
    id: number,
    idKolekcji: number,
    idPliku: number,
    nazwaWlasna: string
}

export interface CutEditModel {
    id: number,
    nazwa: string,
    fileInBase64: string
}
