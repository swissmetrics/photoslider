export interface ResponseModel {
    status: number,
    message: string
}

export interface TabModel {
    id: number,
    idKolekcji: number,
    idPliku: number,
    nazwaWlasna: string
}
