export interface FavoritesModel {
    id: number,
    idPliku: number,
    idKolekcji: number,
    nazwaWlasna: string
}
