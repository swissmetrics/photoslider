import { AfterViewInit, Component, ContentChildren, Directive, ElementRef, Input, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren } from '@angular/core';
import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style } from '@angular/animations';
import { CarouselItemDirective } from '../directives/carousel-item';

@Directive({
    selector: '.carousel-item'
})

export class CarouselItemElement {
}

@Component({
    selector: 'carousel',
    exportAs: 'carousel',
    template: `
        <section class="carousel-outer">
            <a *ngIf="showNext" class="controls next" (click)="next()"></a>
            <section class="carousel-wrapper" #carouselWrapper>
              <ul class="carousel-inner" #carousel>
                <li *ngFor="let item of items;" class="carousel-item">
                  <ng-container [ngTemplateOutlet]="item.tpl"></ng-container>
                </li>
              </ul>
            </section>
            <a *ngIf="showPrev" class="controls prev" (click)="prev()"></a>
        </section>
      `,
      styles: [`
        ul {
          list-style: none;
          margin: 0;
          padding: 0;
          width: 6000px;
        }
        .carousel-wrapper {
          overflow: hidden;
        }
        .carousel-inner {
          display: flex;
        }
      `]
})

export class CarouselComponent implements AfterViewInit {

      @ContentChildren(CarouselItemDirective) items : QueryList<CarouselItemDirective>;
      @ViewChildren(CarouselItemElement, { read: ElementRef }) private itemsElements : QueryList<ElementRef>;
      @ViewChild('carousel') private carousel : ElementRef;
      @ViewChild('carouselWrapper') private carouselWrapper : ElementRef;
      @Input() timing = '250ms ease-in';
      public showNext: boolean = false;
      public showPrev: boolean = false;
      private player : AnimationPlayer;
      private itemWidth : number;
      private currentSlide = 0;

      constructor( private builder : AnimationBuilder ) {
      }

      ngAfterViewInit() {
          setTimeout(() => {
              this.itemWidth = this.itemsElements.first.nativeElement.getBoundingClientRect().width;
              this.showControls();
          });
      }

      showControls() {
          if( this.itemWidth *  this.items.length > this.carouselWrapper.nativeElement.clientWidth ) {
              this.showNext = true;
          }
      }

      private buildAnimation( offset ) {
          return this.builder.build([
              animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
          ]);
      }

      next() {
          this.showNext = true;
          this.showPrev = true;
          let tmpCurrentSlide = (this.currentSlide + 1) % this.items.length;
          const offset = tmpCurrentSlide * this.itemWidth

          if( (this.items.length - this.currentSlide - 1) * this.itemWidth <= this.carouselWrapper.nativeElement.clientWidth ) {
              this.showNext = false;
          }

          this.currentSlide = tmpCurrentSlide;
          const myAnimation : AnimationFactory = this.buildAnimation(offset);
          this.player = myAnimation.create(this.carousel.nativeElement);
          this.player.play();
      }

      prev() {
          this.showNext = true;
          this.showPrev = true;

          if( this.currentSlide === 1 ) {
              this.showPrev = false;
          }
          if( this.currentSlide === 0 ) {
              return;
          }

          this.currentSlide = ((this.currentSlide - 1) + this.items.length) % this.items.length;
          const offset = this.currentSlide * this.itemWidth;

          const myAnimation : AnimationFactory = this.buildAnimation(offset);
          this.player = myAnimation.create(this.carousel.nativeElement);
          this.player.play();
      }
}
