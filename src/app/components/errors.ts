import { Component } from '@angular/core';
import { ConfigService } from '../services/config';

@Component({
  selector: 'errors',
  templateUrl: '../templates/errors.html',
  styleUrls: ['../styles/login.scss']
})

export class ErrorsComponent {

    constructor(public configService: ConfigService) {
    }
}
