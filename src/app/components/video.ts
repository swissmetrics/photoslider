import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../services/photos';
import { ConfigService } from '../services/config';

@Component({
  selector: 'video-detail',
  templateUrl: '../templates/video-detail.html',
  styleUrls: ['../styles/detail.scss']
})

export class VideoComponent implements OnInit {

    constructor(public photosService: PhotosService, public configService: ConfigService) {
    }

    ngOnInit() {
    }
}
