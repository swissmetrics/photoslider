import { Component, DoCheck, OnInit } from '@angular/core';
import { PhotosService } from '../services/photos';
import { MenuSelectService } from '../services/menu-select';
import { ConfigService } from '../services/config';
import {PinModel, CutModel, CutEditModel} from '../models/selected';

@Component({
  selector: 'menu-select',
  templateUrl: '../templates/menu-select.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuSelectComponent implements DoCheck, OnInit {

    public active: string;
    public pinName: string;
    public cutName: string;
    public editPinName: string;
    public editCutName: string;
    public editPinId: number;
    public editCutItem: CutEditModel | null;
    public pins: PinModel[];
    public cuts: CutModel[];
    private initDetaiId: number;
    public numberPinsSelected: number = 0;
    public numberCutsSelected: number = 0;

    constructor(
        public configService: ConfigService,
        public menuSelectService: MenuSelectService,
        public photosService: PhotosService) {
            this.initDetaiId = this.photosService.detail.id
    }

    ngOnInit() {
        this.showPins();
        this.showCuts();
        this.photosService.showSelectedPin = false;
    }

    ngDoCheck() {
        if (this.photosService.detail) {
            if (this.initDetaiId !== this.photosService.detail.id) {
                this.showPins();
                this.showCuts();
                this.initDetaiId = this.photosService.detail.id;
            }
        }
    }

    pin() {
        this.active = (this.active == 'pin') ? '' : 'pin';
        /*let src = (location.href.indexOf('localhost') !== -1) ? '/assets/crc1.jpg' : this.photosService.detail.src;*/
        let src = this.photosService.detail.src;
        if (this.active) {
            this.photosService.showAllPins = false;
            this.photosService.isPin = true;
            this.photosService.photoBase64 = null;
            this.photosService.croppedImage = null;
            this.photosService.showSelectedPin = true;
        }
        else {
          this.photosService.showAllPins = true;
          this.photosService.isPin = false;
          this.photosService.showSelectedPin = false;
        }
    }

    showPins() {
        this.numberPinsSelected = 0;
        this.photosService.pinCoordinates = null;
        this.photosService.pins = null;
        this.menuSelectService
            .getPins(this.photosService.detail.id)
            .subscribe(ret => {
                this.photosService.pins = ret;
                this.numberPinsSelected += ret.length;
            });
    }

    savePin() {
        if (!this.photosService.pinCoordinates) {
            this.configService.error = 'Zaznacz pinezkę.';
            return;
        }
        else if (!this.pinName) {
            this.configService.error = 'Podaj opis pinezki.';
            return;
        }
        this.menuSelectService
            .addToPin(
                this.photosService.detail.id,
                this.photosService.pinCoordinates.left,
                this.photosService.pinCoordinates.top,
                this.pinName)
            .subscribe(
                ret => {
                    this.configService.error = 'Dodano pinezkę.';
                    this.pinName = '';
                    this.showPins();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    editPin(pinId: number) {
        let pick = this.photosService.pins.findIndex(x => x.id === pinId);
        /*this.editPinName = this.editPinName ? '' : this.photosService.pins[pick].opis;*/
        this.editPinName = this.photosService.pins[pick].opis;
        this.editPinId = this.photosService.pins[pick].id;
        this.photosService.pinCoordinates = {top: this.photosService.pins[pick].top, left: this.photosService.pins[pick].left};
        this.photosService.showSelectedPin = true;
        this.editCutItem = null;
    }

    editPinRemove(pinId: number) {
        this.menuSelectService
            .removePin(pinId)
            .subscribe(
                ret => {
                    this.configService.error = 'Usunięto pinezkę.';
                    this.editPinName = '';
                    this.editPinId = null;
                    this.photosService.showSelectedPin = false;
                    this.showPins();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    editPinSave(pinId: number) {
        if (!this.editPinName) {
            this.configService.error = 'Podaj opis pinezki.';
            return;
        }
        this.menuSelectService
            .editPin(pinId, this.editPinName)
            .subscribe(
                ret => {
                    this.configService.error = 'Zmiany zostały zapisane.';
                    this.editPinName = '';
                    this.editPinId = null;
                    this.photosService.showSelectedPin = false;
                    this.showPins();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    showCuts() {
        this.numberCutsSelected = 0;
        this.photosService.pinCoordinates = null;
        this.photosService.cuts = null;
        this.menuSelectService
            .getCuts(this.photosService.detail.id)
            .subscribe(ret => {
                this.photosService.cuts = ret;
                this.numberCutsSelected += ret.length;
            });
    }


    cut() {
      this.photosService.showAllPins = false;
      this.active = (this.active == 'cut') ? '' : 'cut';
        let src = this.photosService.detail.src;
        /*let src = (location.href.indexOf('localhost') !== -1) ? '/assets/crc1.jpg' : this.photosService.detail.src;*/
        if (this.active) {
            this.makeBase64(src);
            this.photosService.isPin = false;
            this.photosService.showAllPins = true;
        }
        else {
            this.photosService.croppedImage = '';
            this.photosService.photoBase64 = null;
            this.photosService.showAllPins = true;
        }

    }

    makeBase64(src: string) {
        let img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = () => {
          let canvas = document.createElement('canvas');
          let ctx = canvas.getContext('2d');
          canvas.height = img.height;
          canvas.width = img.width;
          ctx.drawImage(img, 0, 0);
          let dataURL = canvas.toDataURL('image/jpeg');
          canvas = null;
          this.photosService.photoBase64 = dataURL;
        };
        img.src = src;
    }

    remove() {
        this.active = '';
        this.photosService.croppedImage = null;
        this.photosService.photoBase64 = null;
    }

    save() {
        if (this.active === 'cut') {
            this.saveCut();
        }
    }

    saveCut() {
        if (!this.cutName) {
            this.configService.error = 'Podaj nazwę wycięcia.';
            return;
        }
        this.menuSelectService
            .saveCut(this.photosService.detail.id, this.cutName, this.photosService.croppedImage)
            .subscribe(
                ret => {
                    this.configService.error = 'Wycięcie zostało zapisane.';
                    this.cutName = '';
                    this.showCuts()
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    editCut(cutId: number) {
        this.editPinName = '';
        this.editPinId = null;
        this.photosService.showSelectedPin = false;
        if (!this.editCutItem || this.editCutItem['id'] !== cutId) {
            this.menuSelectService
                .getCut(cutId)
                .subscribe(ret => {
                    // console.log(ret)
                    this.editCutItem = ret;
                });
        } else {
            this.editCutItem = null;
        }
    }

    editCutRemove(cutId: number) {
        this.menuSelectService
            .removeCut(cutId)
            .subscribe(
                ret => {
                    this.configService.error = 'Usunięto wycięcie.';
                    this.editCutItem = null;
                    this.showCuts();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    editCutSave(cutId: number) {
        if (!this.editCutItem.nazwa) {
            this.configService.error = 'Podaj nazwę wycięcia.';
            return;
        }
        this.menuSelectService
            .editCut(cutId, this.editCutItem.nazwa, this.editCutItem.nazwa)
            .subscribe(
                ret => {
                    this.configService.error = 'Zmiany zostały zapisane.';
                    this.editCutItem = null;
                    this.showCuts();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

}
