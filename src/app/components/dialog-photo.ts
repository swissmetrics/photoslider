import { Component, Input } from '@angular/core';
import { PhotosService } from '../services/photos';

@Component({
    selector: 'photo-modal',
    templateUrl: '../templates/dialog-photo.html',
    styleUrls: ['../styles/dialog.scss'],
})

export class DialogPhotoComponent {

    @Input() src: string;

    constructor(public photosService: PhotosService){}

}
