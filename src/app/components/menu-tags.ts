import { Component, OnInit } from '@angular/core';
import {ConfigService} from '../services/config';
import {MenuGeneralService} from '../services/menu-general';

@Component({
  selector: 'menu-tags',
  templateUrl: '../templates/menu-tags.html',
  styleUrls: ['../styles/menu.scss']
})
export class MenuTagsComponent implements OnInit {
  public tags: String[];
  public indexes: String[];

  constructor(
    public configService: ConfigService,
    private menuGeneralService: MenuGeneralService,
  ) { }

  ngOnInit() {
    this.getTags();
  }

  getTags() {
    this.menuGeneralService
      .getTags()
      .subscribe(ret => {
        this.tags = ret;
      });
  }

  getIndexes() {
    this.menuGeneralService
      .getIndexes()
      .subscribe(ret => {
        this.indexes = ret;
      });
  }
}
