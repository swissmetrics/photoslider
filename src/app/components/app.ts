import {Component, OnDestroy, ViewChild, AfterViewChecked, ChangeDetectorRef} from '@angular/core';
import {Routes, ActivatedRoute, Params} from '@angular/router';
import {PhotosService} from '../services/photos';
import {ConfigService} from '../services/config';
import {PhotosModel, CollectionModel} from '../models/photos';
import {DetailComponent} from './detail';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: '../templates/app.html',
  styleUrls: ['../styles/app.scss']
})

export class AppComponent implements OnDestroy, AfterViewChecked {

  @ViewChild(DetailComponent) detailComponent: DetailComponent;
  public items: PhotosModel | CollectionModel;
  public darkBackground: boolean = false;
  public leftMenu: string = '';
  private routeSub: any;

  constructor(
    private photosService: PhotosService,
    private route: ActivatedRoute,
    private configService: ConfigService,
    private changeDetector: ChangeDetectorRef,
    translate: TranslateService
  ) {
    this.routeSub = this.route.queryParams.subscribe((params: Params) => {
      if (Object.keys(params).length) {
        for (let p of Object.keys(params)) {
          this.configService.input[p] = params[p];
        }
        if (this.configService.input.jednostkaid) {
          this.loadItems();
        }
        else if (this.configService.input.kolekcjaid) {
          this.loadCollections();
        }
        else if (this.configService.input.plikid) {
          this.loadFile();
        }
        console.log('translate');
        /*if (this.configService.input.jezyk) translate.use((this.configService.input.jezyk));
        else translate.use('pl_PL');*/

        if (this.configService.input.jezyk == 'pl_PL' || !(this.configService.input.jezyk)) {
          console.log('pl');
          translate.setTranslation('pl', {
            INFORMACJE: 'Informacje',
            ZAZNACZ: 'Zaznacz',
            ZAKLADKA: 'Zakładka',
            DOKOLEKCJI: 'Do kolekcji',
            POBIERZ: 'Pobierz',
            UDOSTEPNIJ: 'Udostepnij',
            DOULUBIONYCH: 'Do ulubionych',
            USUN: 'Usuń',
            TAGI: 'Tagi',
            ZWIN: 'Zwiń',
            PELNYEKRAN: 'Pełny ekran',
            USTAWIENIAJASNOSCI: 'Ustawienia jasności',
            OBROC: 'Obróć',
            DODAJDOZAMOWIENIA: 'Dodaj do zamówienia',
            BROWSER_NOT_SUPPORT_VIDEO: 'Twoja przeglądarka nie wspiera tagów video',
            WROC: 'Wróć ',
            DO_SKANOW: 'do skanów',
            INDEKSY_NADANE_PRZEZ_ARCHIWA: 'Indeksy nadane przez archiwa',
            TAGI_UZYTKOWNIKA: 'tagi użytkownika',
            DODAJ_DO_ZAKLADEK: 'Dodaj do zakładek',
            USUN_Z_ZAKLADEK: 'Usuń z zakładek',
            MENU_TAB_1: 'Możesz dodawać zakładki do pojedynczych skanów, które cię interesują i zapisać je na swoim koncie użytkownika.',
            MENU_TAB_1_2: '',
            MENU_TAB_2: 'Zawsze znajdziesz to co cię interesowało w danej jednosce czy kolekcji.',
            PRZYPNIJ: 'przypnij',
            MOZESZ_PINEZKI_1: 'Możesz przypinać pineski do fragmentów, który chcesz zaznaczyć i zapisać na swoim Koncie użytkownika.',
            MOZESZ_PINEZKI_2: '',
            WYTNIJ: 'wytnij',
            MOZESZ_WYCINAC: 'Możesz wycinać fragmenty, które cię interesują i zapisać na swoim Koncie użytkownika.',
            ZAPISZ: 'zapisz',
            TAGI_DLA_OBIEKTU: 'Tagi dla obiektu',
            POBIERZ_PLIK: 'Pobierz plik',
            POBIERZ_JPG: 'Pobierz w jpg',
            POBIERZ_JPG_XML: 'Pobierz w jpg i xml',
            MOZESZ_FOLDERY_1: 'Możesz tworzyć dowolne foldery i podfoldery, w których możesz zapisywać ',
            MOZESZ_FOLDERY_2: 'i porządkować pojedyncze skany i całe jednostki.',
            DODAJ_DO_FOLDERU: 'Dodaj do folderu',
            OPCJA_PO_ZALOGOWANIU: 'Opcja dostępna po zalogowaniu',
            LUB_NIE: 'Lub jeśli nie posiadasz konta',
            BROWSER_NOT_SUPPORT_AUDIO: 'Twoja przeglądarka nie wspiera elementów audio',
            OPIS_SZCZEGOLOWY: 'Opis szczegółowy',
            DATA_OKRES_POWSTANIA: 'Dat/Okres powstania',
            AUTOR: 'Autor',
            PRAWA: 'Prawa',
            ZGLOS_BLAD_MATERIALU: 'Zgłoś błąd materiału',
            ZALOGUJ_SIE: 'Zaloguj się',
            ZALOZ_KONTO: 'Zalóż konto',
            ZAZNACZENIE: 'Zaznaczenie',
            ZAKLADKI: 'Zakładki',
            TWOJE_FOLDERY: 'Twoje foldery',
            SKAN: 'Skan',
            SKAN_Z: 'z',
            LINK: 'Link do skanu',
            ALT_JEDNOSTKA: 'Obraz z jednostki',
            ALT_KOLEKCJA: 'Obraz z kolekcji',
            ALT_NUMER: 'o numerze'
          });
          translate.use('pl');
        }

        if (this.configService.input.jezyk == 'ru_RU') {
          console.log('ru');
          translate.setTranslation('ru', {
            INFORMACJE: 'Информации',
            ZAZNACZ: 'Выбрать',
            ZAKLADKA: 'Закладка',
            DOKOLEKCJI: 'К коллекции',
            POBIERZ: 'Скачать',
            UDOSTEPNIJ: 'Поделиться',
            DOULUBIONYCH: 'В избранное',
            USUN: 'Удалить',
            TAGI: 'Теги',
            ZWIN: 'Свернуть',
            PELNYEKRAN: 'Полный экран',
            USTAWIENIAJASNOSCI: 'Настройки яркости',
            OBROC: 'Повернуть',
            DODAJDOZAMOWIENIA: 'Добавить к заказа',
            BROWSER_NOT_SUPPORT_VIDEO: 'Ваш браузер не поддерживает тега видео',
            WROC: 'Назад',
            DO_SKANOW: 'Вернитесь к сканированию',
            INDEKSY_NADANE_PRZEZ_ARCHIWA: 'Индексы данные архивами',
            TAGI_UZYTKOWNIKA: 'Пользовательские теги',
            DODAJ_DO_ZAKLADEK: 'Добавить к закладкам',
            USUN_Z_ZAKLADEK: 'Удаление из закладок',
            MENU_TAB_1: 'Вы можете добавлять закладки к отдельным санированиям, которые вас интересуют, .',
            MENU_TAB_1_2: 'и сохранять их в своем аккаунте пользователя.',
            MENU_TAB_2: 'Вы всегда найдете то, что вас интересует в данной единицы или коллекции.',
            PRZYPNIJ: 'Прикреплять',
            MOZESZ_PINEZKI_1: 'Вы можете прикреплять булавки к фрагментам, которые вы хотите выбрать и сохранить в в своем аккаунте ',
            MOZESZ_PINEZKI_2: 'пользователя.',
            WYTNIJ: 'Вырезать',
            MOZESZ_WYCINAC: 'Вы можете обрезать фрагменты, которые вас интересуют, и сохранить в своем аккаунте пользователя.',
            ZAPISZ: 'Сохранить',
            TAGI_DLA_OBIEKTU: 'Теги для объекта',
            POBIERZ_PLIK: 'Скачать файл',
            POBIERZ_JPG: 'Скачать в jpg',
            POBIERZ_JPG_XML: 'Скачать в jpg и xml',
            MOZESZ_FOLDERY_1: 'Вы можете создавать любые папки и подпапки, в которых вы можете сохранять и организовывать ',
            MOZESZ_FOLDERY_2: 'отдельные сканы и целые единицы',
            DODAJ_DO_FOLDERU: 'Добавить в папку',
            OPCJA_PO_ZALOGOWANIU: 'Опция доступна после входа в систему',
            LUB_NIE: 'Или если у вас нет аккаунта',
            BROWSER_NOT_SUPPORT_AUDIO: 'Ваш браузер не поддерживает аудио элемент.',
            OPIS_SZCZEGOLOWY: 'Подробное описание',
            DATA_OKRES_POWSTANIA: 'Дата / Период создания',
            AUTOR: 'Автор',
            PRAWA: 'Права',
            ZGLOS_BLAD_MATERIALU: 'Сообщить об ошибке материала',
            ZALOGUJ_SIE: 'Zaloguj się',
            ZALOZ_KONTO: 'Zalóż konto',
            ZAZNACZENIE: 'Zaznaczenie',
            ZAKLADKI: 'Zakładki',
            TWOJE_FOLDERY: 'Twoje foldery',
            SKAN: 'Skan',
            SKAN_Z: '',
          });
          translate.use('ru');
        }

        if (this.configService.input.jezyk == 'en_US') {
          console.log('en');
          translate.setTranslation('en', {
            INFORMACJE: 'Information',
            ZAZNACZ: 'Check',
            ZAKLADKA: 'Bookmark',
            DOKOLEKCJI: 'To the collection',
            POBIERZ: 'Download',
            UDOSTEPNIJ: 'Share',
            DOULUBIONYCH: 'To favorites',
            USUN: 'Delete',
            TAGI: 'Tags',
            PELNYEKRAN: 'Full screen',
            USTAWIENIAJASNOSCI: 'Brightness settings',
            OBROC: 'Turn',
            DODAJDOZAMOWIENIA: 'Add orders',
            ZWIN: 'Roll',
            BROWSER_NOT_SUPPORT_VIDEO: 'Your browser does not support the video tag.',
            WROC: 'GO back ',
            DO_SKANOW: 'to scans',
            INDEKSY_NADANE_PRZEZ_ARCHIWA: 'Indexes given by archives',
            TAGI_UZYTKOWNIKA: 'User tags',
            DODAJ_DO_ZAKLADEK: 'Add to bookmarks',
            USUN_Z_ZAKLADEK: 'Delete from bookmarks',
            MENU_TAB_1: 'You can add bookmarks to individual scans that interest you and save them to your user account.',
            MENU_TAB_1_2: '',
            MENU_TAB_2: 'You will always find what interests you in a given unit or collection.',
            PRZYPNIJ: 'Pin up',
            MOZESZ_PINEZKI_1: 'You can attach pins to the fragments you want to select and save in your User Account.',
            MOZESZ_PINEZKI_2: '',
            WYTNIJ: 'Cut',
            MOZESZ_WYCINAC: 'You can cut fragments that interest you and save in your User Account.',
            ZAPISZ: 'Save',
            TAGI_DLA_OBIEKTU: 'Tags for the object',
            POBIERZ_PLIK: 'Download file',
            POBIERZ_JPG: 'Download in jpg',
            POBIERZ_JPG_XML: 'Download in jpg and xml',
            MOZESZ_FOLDERY_1: 'You can create any folders and subfolders in which you can save and ',
            MOZESZ_FOLDERY_2: 'organize individual scans and whole units',
            DODAJ_DO_FOLDERU: 'Add to folder',
            OPCJA_PO_ZALOGOWANIU: 'Option available after logging in',
            LUB_NIE: 'Or if you do not have an account',
            BROWSER_NOT_SUPPORT_AUDIO: 'Your browser does not support the audio element.',
            OPIS_SZCZEGOLOWY: 'Detailed description',
            DATA_OKRES_POWSTANIA: 'Date / Period of creation',
            AUTOR: 'Author',
            PRAWA: 'Rights',
            ZGLOS_BLAD_MATERIALU: 'Report error of materia',
          });
          translate.use('en');
        }

        if (this.configService.input.jezyk == 'de_DE') {
          console.log('de');
          translate.setTranslation('de', {
            INFORMACJE: 'Information',
            ZAZNACZ: 'Prüfen',
            ZAKLADKA: 'Tab',
            DOKOLEKCJI: 'Zur Sammlung',
            POBIERZ: 'Download',
            UDOSTEPNIJ: 'Teilen',
            DOULUBIONYCH: 'Zu den Favoriten',
            USUN: 'Löschen',
            TAGI: 'Tags',
            ZWIN: 'Rollen',
            PELNYEKRAN: 'Vollbildschirm',
            USTAWIENIAJASNOSCI: 'Helligkeitseinstellungen',
            OBROC: 'Wenden',
            DODAJDOZAMOWIENIA: 'Bestellungen hinzufügen',
            BROWSER_NOT_SUPPORT_VIDEO: 'Ihr Browser unterstützt das Video-Tag nicht.',
            WROC: 'Rückkehr',
            DO_SKANOW: 'Gehen Sie zurück zu den Scans',
            INDEKSY_NADANE_PRZEZ_ARCHIWA: 'Indizes durch die Archive gegeben',
            TAGI_UZYTKOWNIKA: 'Benutzer-Tags',
            DODAJ_DO_ZAKLADEK: 'Zu Lesezeichen hinzufügen',
            USUN_Z_ZAKLADEK: 'Aus Lesezeichen entfernen',
            MENU_TAB_1: 'Sie können einzelne Scans, die Sie interessieren, mit Lesezeichen versehen und in Ihrem Benutzerkonto speichern.',
            MENU_TAB_1_2: '',
            MENU_TAB_2: 'Sie werden immer das finden, was Sie an einer bestimmten Einheit oder Sammlung interessiert.',
            PRZYPNIJ: 'anfügen',
            MOZESZ_PINEZKI_1: 'Sie können Stecknadeln an die Fragmente anfügen, die Sie auswählen und in ',
            MOZESZ_PINEZKI_2: 'Ihrem Benutzerkonto speichern möchten.',
            WYTNIJ: 'Schnitt',
            MOZESZ_WYCINAC: 'Sie können Fragmente ausschneiden, die Sie interessieren, und in Ihrem Benutzerkonto speichern.',
            ZAPISZ: 'speichern',
            TAGI_DLA_OBIEKTU: 'Tags für das Objekt',
            POBIERZ_PLIK: 'Datei herunterladen',
            POBIERZ_JPG: 'In .jpg herunterladen',
            POBIERZ_JPG_XML: 'Es wird in .jpg und .xml heruntergeladen',
            MOZESZ_FOLDERY_1 : 'Sie können beliebige Ordner und Unterordner erstellen, in denen Sie einzelne ',
            MOZESZ_FOLDERY_2 : 'Scans und ganze Einheiten speichern und organisieren können.',
            DODAJ_DO_FOLDERU: 'Zu Ordner hinzufügen',
            OPCJA_PO_ZALOGOWANIU: 'Die Option ist nach dem Einloggen verfügbar',
            LUB_NIE: 'Oder wenn Sie noch kein Konto haben',
            BROWSER_NOT_SUPPORT_AUDIO: 'Ihr Browser unterstützt das Audioelement nicht.',
            OPIS_SZCZEGOLOWY: 'Ausführliche Beschreibung',
            DATA_OKRES_POWSTANIA: 'Datum/Zeitraum der Erstellung',
            AUTOR: 'Autor',
            PRAWA: 'Rechte',
            ZGLOS_BLAD_MATERIALU: 'Materialfehler melde',
          });
          translate.use('de');
        }

          this.auth();
        }
      });

  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  auth() {
    this.photosService
      .login()
      .subscribe(ret => {
        if (ret.message === 'OnLine') {
          this.configService.isLogged = true;
        }
      });
  }

  loadItems() {
    this.photosService
      .getItems(this.configService.input.jednostkaid)
      .subscribe(ret => {
        this.items = ret;
        this.fileInside();
      });
  }

  loadCollections() {
    this.photosService
      .getCollections(this.configService.input.kolekcjaid)
      .subscribe(ret => {
        this.items = ret;
        this.fileInside();
      });
  }

  fileInside() {
    if (this.configService.input.plikid) {
      for (let e in this.items.elementy) {
        if (this.items.elementy[e].id == this.configService.input.plikid) {
          this.photosService.photoOrder = +e;
          break;
        }
      }
    }
  }

  loadFile() {
    this.photosService
      .getItem(this.configService.input.plikid)
      .subscribe(ret => {
        this.items = {
          jednostka_id: 0,
          jednostka_nazwa: ret.nazwa,
          elementy: [ret],
          objekt_id: ret.objekt_id
        };
      });
  }

  onFooterState(event) {
    this.darkBackground = event.state;
  }

  onClickFooterFoto(event) {
    this.detailComponent.getDetail(event.id);
  }

  onMenuState(event) {
    this.leftMenu = event.state;
  }

  onFullSize() {
    this.detailComponent.showFullSize();
  }

  onZoom(event) {
    this.detailComponent.zoomPhoto(event.zoom);
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
