import { Component, Input } from '@angular/core';

@Component({
  selector: 'ps-top',
  templateUrl: '../templates/top.html',
  styleUrls: ['../styles/top.scss']
})
export class TopComponent {
    @Input() title: string;
}
