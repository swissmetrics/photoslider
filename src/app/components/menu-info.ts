import { Component } from '@angular/core';
import { PhotosService } from '../services/photos';
import { MenuGeneralService } from '../services/menu-general';
import { ConfigService } from '../services/config';

@Component({
  selector: 'menu-info',
  templateUrl: '../templates/menu-info.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuInfoComponent {

    public showReport: number[] = [];
    public showForm: number[] = [];
    public reportMsg: string;

    constructor(
        public configService: ConfigService,
        private menuGeneralService: MenuGeneralService,
        public photosService: PhotosService) {
    }

    report(): void {
        if (!this.reportMsg) {
            this.configService.error = 'Wpisz treść.';
            return;
        }
        this.menuGeneralService
            .sendMessage(this.photosService.detail.id, this.reportMsg)
            .subscribe(
                ret => {
                    this.configService.error = 'Wiadomość została wysłana.';
                    this.showReport.push(this.photosService.detail.id);
                    let pick = this.showForm.indexOf(this.photosService.detail.id);
                    this.showForm.splice(pick, 1);
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    showFormClick() {
        this.reportMsg = '';
        this.showForm.push(this.photosService.detail.id);
    }
}
