import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../services/photos';
import { ConfigService } from '../services/config';
import {MenuSelectService} from '../services/menu-select';

@Component({
  selector: 'menu-download',
  templateUrl: '../templates/menu-download.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuDownloadComponent implements OnInit {

    public fileXml: string;
    public downloadJPEG: string;
    private initDetaiId: number;

    constructor(
      public menuSelectService: MenuSelectService,
      public photosService: PhotosService,
      public configService: ConfigService) {
        this.initDetaiId = this.photosService.detail.id
    }

    ngOnInit() {
      this.fileXml = '/o/pliki-api/pliki/pobierzplikixml/';
      this.downloadJPEG = '/o/pliki-api/pliki/pobierzplikjpeg/';
    }
}
