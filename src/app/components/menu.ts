import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PhotosService } from '../services/photos';
import { MenuFavoritesService } from '../services/menu-favorites';
import { ConfigService } from '../services/config';

@Component({
  selector: 'ps-manu',
  templateUrl: '../templates/menu.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuComponent implements OnInit {

    @Output() menuState = new EventEmitter();
    public showMore: string = '';
    public addedFavorites: number[] = [];

    constructor(
        private configService: ConfigService,
        private menuFavoritesService: MenuFavoritesService,
        public photosService: PhotosService) {
    }

    ngOnInit() {
        this.loadFavorites();
    }

    // load all user favorites
    loadFavorites() {
        this.menuFavoritesService
            .getAllFavorites()
            .subscribe(ret => {
                this.menuFavoritesService.userFavorites = ret;
            });
    }

    hasObject() {
      if (this.configService.input.objektid != null) {
        return true;
      } else {
        return false;
      }
    }

    showManu(kind: string) {
        if (kind === 'select') {
          this.photosService.showAllPins = true;
        } else {
          this.photosService.showAllPins = false;
        }
        this.showMore = kind;
        if (kind === 'hide') {
            this.showMore = '';
        }
        this.menuEmit();
        this.photosService.photoBase64 = null;
        this.photosService.pinCoordinates = null;
        this.photosService.isPin = false;
    }

    menuEmit() {
        let e = {state: this.showMore};
        this.menuState.emit(e);
    }

    add() {
        if (!this.configService.isLogged) {
            this.showManu('favorites');
            return;
        }
        let pick = this.menuFavoritesService.userFavorites.findIndex(x => x.idPliku === this.photosService.detail.id);
        if (pick !== -1) {
            this.removeFromFavorites()
        } else {
            this.addToFavorites();
        }
    }

    addToFavorites() {
        this.menuFavoritesService
            .addFavorites(this.photosService.detail.id)
            .subscribe(
                ret => {
                    this.configService.error = 'Dodano do ulubionych.';
                    this.loadFavorites();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    removeFromFavorites() {
        let favoritesId;
        for (let f of this.menuFavoritesService.userFavorites) {
            if (f.idPliku === this.photosService.detail.id) {
                favoritesId = f.id;
                break;
            }
        }
        this.menuFavoritesService
            .removeFavorites(favoritesId)
            .subscribe(
                ret => {
                    this.configService.error = 'Usunieto z ulubionych.';
                    this.loadFavorites();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    // check if scan is added to favorites
    isExist(): boolean {
        let find = this.menuFavoritesService.userFavorites.findIndex(x => x.idPliku === this.photosService.detail.id);
        if (find !== -1) {
            return true;
        }
        return false;
    }
}
