import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../services/photos';
import { ConfigService } from '../services/config';

@Component({
  selector: 'audio-detail',
  templateUrl: '../templates/audio-detail.html',
  styleUrls: ['../styles/detail.scss']
})

export class AudioComponent implements OnInit {

    constructor(public photosService: PhotosService, public configService: ConfigService) {
    }

    ngOnInit() {
    }
}
