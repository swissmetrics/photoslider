import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PhotosModel } from '../models/photos';
import { PhotosService } from '../services/photos';
import { ConfigService } from '../services/config';
import {TranslateService} from '@ngx-translate/core'

@Component({
  selector: 'ps-footer',
  templateUrl: '../templates/footer.html',
  styleUrls: ['../styles/footer.scss']
})

export class FooterComponent implements OnInit {

    @Input() photos: PhotosModel;
    @Output() footerState = new EventEmitter();
    @Output() clickedPhoto = new EventEmitter();
    @Output() fullSize = new EventEmitter();
    @Output() zoom = new EventEmitter();
    public showCarousel: boolean = false;
    public photoOrder: number = 1;
    public altText: string;
    public altNumer: string;

    constructor(
        public configService: ConfigService,
        public photosService: PhotosService,
        public translateService: TranslateService,
        private router: Router) {}

    ngOnInit() {
      this.altNumer = this.translateService.instant('ALT_NUMER');
      if (this.configService.input.kolekcjaid !== null) {
        this.altText = this.translateService.instant('ALT_KOLEKCJA');
      } else {
        this.altText = this.translateService.instant('ALT_JEDNOSTKA');
      }
    }

    onFooter() {
        this.showCarousel = !this.showCarousel;
        let e = {state: this.showCarousel};
        this.footerState.emit(e);
    }

    photoClick(id: number, order: number) {
        this.photosService.photoOrder = order;
        this.clickedPhoto.emit( {id: id} );
    }

    fullSizeClick() {
        this.fullSize.emit();
    }

    zoomClick(size: number) {
        // disable selected options
        this.photosService.isPin = null;
        this.photosService.photoBase64 = null;
        this.photosService.croppedImage = null;
        this.photosService.setZoom = size;
        this.zoom.emit( {zoom: this.photosService.zoom} );
    }

    rotateClick() {
        this.photosService.rotate += 90;
    }

    buy() {
        this.photosService
            .addToBasket(this.photosService.detail.id)
            .subscribe(
                ret => {
                    this.configService.error = 'Dodano do zamówienia.';
                },
                error => {
                    this.configService.error = 'Coś poszło nie tak.';
                }
            );
    }

    changeRange(value: number) {
        this.photosService.brightness = value;
    }

    isAudioOrVideo() {
      if (this.photosService.detail.typ === 'audio' || this.photosService.detail.typ === 'video') {
        return true;
      }
    }
}
