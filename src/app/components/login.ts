import { Component } from '@angular/core';
import { ConfigService } from '../services/config';

@Component({
  selector: 'login',
  templateUrl: '../templates/login.html',
  styleUrls: ['../styles/login.scss']
})

export class LoginComponent {

    constructor(public configService: ConfigService) {
    }

    login() {
        window.location.href = window.location.origin + '/c/portal/login'
    }

    register() {
        window.location.href = window.location.origin + '/rejestracja'
    }
}
