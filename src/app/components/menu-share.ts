import { Component } from '@angular/core';
import { PhotosService } from '../services/photos';
import { ConfigService } from '../services/config';

@Component({
  selector: 'menu-share',
  templateUrl: '../templates/menu-share.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuShareComponent {


    constructor(public photosService: PhotosService, public configService: ConfigService) {
    }

    share(social: string) {

        let url = {
            facebook: 'https://www.facebook.com/sharer/sharer.php?u=',
            google: 'https://plus.google.com/share?url=',
            twitter: 'https://twitter.com/intent/tweet?url=',
        };

        window.open(
            url[social] + this.photosService.detail.src,
            'Udostępnij',
            'toolbar=1, scrollbars=1, location=1, statusbar=0, menubar=1, resizable=1, width=600, height=400'
        );
    }
}
