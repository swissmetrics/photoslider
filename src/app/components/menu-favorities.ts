import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../services/photos';
import { ConfigService } from '../services/config';

@Component({
  selector: 'menu-favorites',
  templateUrl: '../templates/menu-favorites.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuFavoritiesComponent implements OnInit {

    constructor(public photosService: PhotosService, public configService: ConfigService) {
    }

    ngOnInit() {
    }
}
