import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../services/photos';
import { MenuGeneralService } from '../services/menu-general';
import { ConfigService } from '../services/config';
import { TabModel } from '../models/general';

@Component({
  selector: 'menu-tab',
  templateUrl: '../templates/menu-tab.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuTabComponent implements OnInit {

    public nextTab: number = 0;
    public tabName: string;
    public numberTabs: number = 0;
    public tabs: TabModel[];
    public addedTabs: number[] = [];
    public hasTab: boolean = false;

    constructor(
        public configService: ConfigService,
        private menuGeneralService: MenuGeneralService,
        public photosService: PhotosService) {
    }

    ngOnInit() {
        this.getTabs();
    }

    getTabs() {
        this.menuGeneralService
            .getTabs()
            .subscribe(ret => {
                this.tabs = ret;
                this.numberTabs = ret.length;
                for (let tab of ret) {
                    this.addedTabs.push(tab.idPliku);
                }
              let pick = this.tabs.findIndex(x => x.idPliku === this.photosService.detail.id);
              if (pick !== -1) {
                this.hasTab = true;
              } else {
                this.hasTab = false;
              }
            });
    }

    add() {
        if (this.tabName == null) {
          this.tabName = 'Zakładka-' + this.photosService.detail.id;
        }
        this.menuGeneralService
            .addToTab(this.photosService.detail.id, this.tabName)
            .subscribe(
                ret => {
                    this.configService.error = 'Dodano do zakładek.';
                    this.numberTabs++;
                    this.addedTabs.push(this.photosService.detail.id);
                    this.getTabs();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }

    remove() {
        let pick = this.tabs.findIndex(x => x.idPliku === this.photosService.detail.id);
        this.menuGeneralService
            .removeTab(this.tabs[pick].id)
            .subscribe(
                ret => {
                    this.configService.error = 'Usunięto zakładkę.';
                    this.numberTabs--;
                    this.getTabs();
                },
                error => {
                    this.configService.error = this.configService.failMessage;
                }
            );
    }
}
