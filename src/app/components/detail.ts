import {Component, Input, OnInit, ElementRef, HostListener} from '@angular/core';
import { PhotosService } from '../services/photos';
import { PhotosModel, PhotoModel } from '../models/photos';
import { MenuSelectComponent } from './menu-select';;
import {TranslateService} from '@ngx-translate/core';
import {ConfigService} from '../services/config';


@Component({
  selector: 'ps-detail',
  templateUrl: '../templates/detail.html',
  styleUrls: ['../styles/detail.scss']
})

export class DetailComponent implements OnInit {

    @Input() photos: PhotosModel;
    public photo: PhotoModel;
    public modalPhoto: boolean = false;
    public zoomScale: number;
    public initialZoom: number = 100;
    private index: number;
    public cropperReady = false;
    public audioPlayer;
    public audioDuration;
    public audioTimeline;
    public audioTimelineWidth;
    public audioPosition;
    public pButton;
    public onplayhead = false;
    public dragVolume = false;
    public audioVolumeContainer;
    public audioVolume;
    public audioSpeaker;
    public altText: string;

    private isDown;
    private dialogBoxDiv;
    private img;
    private xPos;
    private yPos;
    private xPosClick;
    private yPosClick;

    constructor(
      public configService: ConfigService,
      public photosService: PhotosService,
      public translateService: TranslateService,) {
        this.zoomScale = this.initialZoom;
        this.index = this.photosService.photoOrder ? this.photosService.photoOrder : 0;
        this.isDown = false;
        this.xPos = 0;
        this.yPos = 0;

    }

    ngOnInit() {
        if (this.photos) {
            let item = this.photos.elementy[this.index];
            this.getDetail(item.id);
            this.dialogBoxDiv = document.getElementsByTagName('figure');
            this.img = document.getElementsByTagName('img');
        }
      if (this.configService.input.kolekcjaid !== null) {
        this.altText = this.translateService.instant('ALT_KOLEKCJA');
      } else {
        this.altText = this.translateService.instant('ALT_JEDNOSTKA');
      }
    }

    imageCroppedBase64(image: string) {
        this.photosService.croppedImage = image;
        this.croppedCoordinates();
    }

    @HostListener('document:keydown', ['$event'])
    arrowEventHandler(event: KeyboardEvent) {
      if (event.key === 'ArrowRight') {
        this.changePhoto(1);
      }
      if (event.key === 'ArrowLeft') {
        this.changePhoto(-1);
      }
      if (!event.shiftKey && event.key === 'Tab' && document.getElementById('dodajDoZamowienia') === document.activeElement) {
        document.getElementById('closeButton').focus();
        event.preventDefault();
      }
      if (event.shiftKey && event.key === 'Tab' && document.getElementById('closeButton') === document.activeElement) {
        document.getElementById('dodajDoZamowienia').focus();
        event.preventDefault();
      }
      if (event.key === 'Escape') {
        document.getElementById('closeButton').click();
      }
    }

    croppedCoordinates(){
        let cropper = <HTMLElement>document.querySelector('.cropper');
        this.photosService.coordinates = {
            top: cropper.offsetTop,
            left: cropper.offsetLeft,
            width: cropper.offsetWidth,
            height: cropper.offsetHeight
        };
    }

    imageLoaded() {
      this.cropperReady = true;
    }

    imageLoadFailed () {
      // console.log('Image load failed');
    }

    changePhoto(position: number) {
        let lastIndex = this.photos.elementy.length - 1;
        let current = this.index + position;
        if (current < 0) {
            current = lastIndex;
        }
        if (current > lastIndex) {
            current = 0;
        }
        let item = this.photos.elementy[current];
        this.index = current;
        this.photosService.photoOrder = this.index;
        this.getDetail(item.id);
    }

    getDetail(id: number) {
        delete this.photo;
        this.photosService
            .getItem(id)
            .subscribe(ret => {
                this.photo = ret;
                this.index = this.photosService.photoOrder;
                this.photosService.detail = ret;
                this.resetFooterSetting();
                // console.log(this.photo)
            });
    }

    resetFooterSetting(){
        this.photosService.zoom = this.initialZoom;
        this.zoomScale = this.initialZoom;
        this.photosService.brightness = 100;
        this.photosService.rotate = 0;
    }

    // do not use
    zoom(e: any){
        let zoomer = e.currentTarget;
        let offsetX = e.offsetX ;
        let offsetY = e.offsetY ;
        let x = offsetX / zoomer.offsetWidth * 100
        let y = offsetY / zoomer.offsetHeight * 100
        this.zoomScale = this.initialZoom;
        this.photosService.zoom = this.initialZoom;
        zoomer.style.backgroundPosition = x + '% ' + y + '%';
    }

    scroll(e: any) {
        if (e.deltaY > 0) {
            this.photosService.setZoom = +this.photosService.zoomStep;
        } else {
            this.photosService.setZoom = -this.photosService.zoomStep;
        }
        this.zoomScale = this.photosService.zoom;
    }

    showFullSize() {
        this.modalPhoto = true;
    }

    zoomPhoto(zoom: number) {
        this.zoomScale = zoom;
    }

    putPin(e: any) {
        this.photosService.pinCoordinates = {
            top: e.offsetY,
            left: e.offsetX
        };
    }

    mousedown($event) {
      this.isDown = true;
      var backgroundPosX = this.dialogBoxDiv[0].style.backgroundPositionX;
      var backgroundPosY = this.dialogBoxDiv[0].style.backgroundPositionY;
      this.xPos = parseInt(backgroundPosX, 10);
      this.yPos = parseInt(backgroundPosY, 10);
      this.xPosClick = $event.offsetX;
      this.yPosClick = $event.offsetY;
    }

    mouseup($event) {
      this.isDown = false;
    }

    mousemove($event) {
      $event.preventDefault();

      if (this.isDown) {
        var mousePosition = {
          x : $event.offsetX,
          y : $event.offsetY
        };

          this.dialogBoxDiv[0].style.backgroundPositionX = (this.xPos + mousePosition.x - this.xPosClick) + 'px';
          this.dialogBoxDiv[0].style.backgroundPositionY = (this.yPos + mousePosition.y - this.yPosClick) + 'px';
      }
    }

  setAudioVideo() {
    if (this.photo.typ === 'audio') {
      this.audioPlayer = document.getElementById('player');
      this.audioDuration = this.audioPlayer.duration;
      this.audioPosition = document.getElementById('playhead');
      this.audioTimeline = document.getElementById('timeline')
      this.pButton = document.getElementById('pButton');
      this.audioTimelineWidth = this.audioTimeline.offsetWidth - this.audioPosition.offsetWidth;

      this.audioVolumeContainer = document.getElementById('volume_control');
      this.audioVolume = document.getElementById('rngVolume');
      this.audioSpeaker = document.getElementById('speaker');

      this.setTotalTime();
    }
  }

  playThrough() {
    this.audioDuration = this.audioPlayer.duration;
  }

  timeUpdate() {
    let playPercent = this.audioTimelineWidth * (this.audioPlayer.currentTime / this.audioDuration);
    this.audioPosition.style.marginLeft = playPercent + 'px';
    document.getElementById('playheadWatch').style.width = playPercent + 'px';

    this.setTimeElapsed();
  }

  setTimeElapsed() {
    let seconds = Math.floor(this.audioPlayer.currentTime % 60);
    let minutes = Math.floor(this.audioPlayer.currentTime / 60);

    document.getElementById('elapsedTime').innerHTML = ('0' + minutes).substr(-2) + ':' + ('0' + seconds).substr(-2);
  }

  setTotalTime() {
    let seconds = Math.floor(this.audioPlayer.duration % 60);
    let minutes = Math.floor(this.audioPlayer.duration / 60);

    document.getElementById('totalTime').innerHTML = ('0' + minutes).substr(-2) + ':' + ('0' + seconds).substr(-2);
  }

  playAudio() {
    if (this.audioPlayer.paused) {
        this.audioPlayer.play();
        this.pButton.className = 'pause';
    } else {
        this.audioPlayer.pause();
        this.pButton.className = 'play';
    }
  }

  moveAudio($event) {
    this.moveplayhead($event);
    this.audioPlayer.currentTime = this.audioDuration * this.clickPercent($event);
  }

  draggableSlider($event) {
    if (this.onplayhead === true ) {
      this.moveplayhead($event);
    }
    if (this.dragVolume === true ) {
      this.volumeListener($event);
    }
  }

  volumeListener($event) {
    if (this.dragVolume === true ) {
      this.updateVolumeBar($event.clientX);
    }
  }

  moveplayhead($event) {
    if (this.onplayhead === true) {
      let newMargLeft = $event.clientX - this.getPosition(this.audioTimeline);

      if (newMargLeft >= 0 && newMargLeft <= this.audioTimelineWidth) {
        this.audioPosition.style.marginLeft = newMargLeft + 'px';
      }

      if (newMargLeft < 0) {
        this.audioPosition.style.marginLeft = '0px';
      }

      if (newMargLeft > this.audioTimelineWidth) {
        this.audioPosition.style.marginLeft = this.audioTimelineWidth + 'px';
      }
    }
  }

  mouseDown($event) {
    this.onplayhead = true;
  }

  mouseUp($event) {
    if (this.onplayhead === true) {
      this.moveplayhead($event);
      this.audioPlayer.currentTime = this.audioDuration * this.clickPercent($event);
    }
    this.onplayhead = false;
    this.dragVolume = false;
  }

  clickPercent($event) {
    return (($event.clientX - this.getPosition(this.audioTimeline)) / this.audioTimelineWidth);
  }

  getPosition(el) {
    return el.getBoundingClientRect().left;
  }

  setVolume(vol) {
    this.audioPlayer.volume = vol.currentTarget.value;
  }

  changeVolume() {
    this.dragVolume = true;
  }

  updateVolumeBar(ev) {
   let position = ev - this.audioVolumeContainer.offsetLeft;
   let percentage = 100 * position / this.audioVolumeContainer.clientWidth;

   if (percentage > 100) {
     percentage = 100;
   }

   if (percentage < 0) {
     percentage = 0;
   }

   this.audioVolume.style.width = percentage + '%';
   this.audioPlayer.volume = percentage / 100;
  }

  muted() {
    if (this.audioPlayer.volume === 0) {
        this.audioPlayer.volume = 1;
        this.audioSpeaker.className = 'speaker';
        this.audioVolume.style.width = '100%';
    } else {
        this.audioPlayer.volume = 0;
        this.audioSpeaker.className = 'mute';
        this.audioVolume.style.width = '0%';
    }
  }
}
