import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { TreeModel, NodeEvent, Ng2TreeSettings } from 'ng2-tree';
import { PhotosService } from '../services/photos';
import { MenuCollectionsService } from '../services/menu-collections';
import { ConfigService } from '../services/config';
import { FoldersModel } from '../models/photos';

@Component({
  selector: 'menu-collection',
  templateUrl: '../templates/menu-collection.html',
  styleUrls: ['../styles/menu.scss']
})

export class MenuCollectionComponent implements AfterViewInit {

    public tree: TreeModel;
    public treeSettings: Ng2TreeSettings = {
      rootIsVisible: false
    };
    public noFolders: number = 0;
    private oopNodeController: any;
    private selectedNode: string | number;

    @ViewChild('treeComponent') treeComponent;

    constructor(
        public configService: ConfigService,
        private menuCollectionsService: MenuCollectionsService,
        public photosService: PhotosService) {
    }

    ngAfterViewInit() {
        if (this.configService.input.user) {
            this.loadFolders();
        }
    }

    loadFolders() {
        let newChildren = [];
        this.noFolders = 0;
        this.menuCollectionsService
            .getFolders()
            .subscribe(ret => {
                for (let r of ret) {
                    let newSubChildren = [];
                    this.noFolders++;
                    if (typeof r.children !== 'undefined') {
                        for (let child of r.children) {
                            newSubChildren.push({value: child.name, id: child.id});
                            this.noFolders++;
                        }
                    }
                    newChildren.push({value: r.name, id: r.id, children: newSubChildren});
                }
                this.tree = {
                    settings: {
                        static: false,
                        rightMenu: false,
                        leftMenu: false
                    },
                    value: '',
                    children: newChildren
                };
            });
    }

    logEvent(e: NodeEvent): void {
        this.oopNodeController = this.treeComponent.getControllerByNodeId(e.node.id);
        this.selectedNode = e.node.id;
    }

    addFolder() {
        let tmpId = Math.random();
        if (this.noFolders < 1 || !this.selectedNode) {
            this.tree = {
              value: '',
              children: [{value: '', id: tmpId}]
            };
            this.setName(tmpId);
        }
        else if (this.oopNodeController) {
            this.oopNodeController.addChild({value: '', id: tmpId});
            this.setName(tmpId);
        }
    }

    setName(nodeId: number) {
        setTimeout(()=>{
            let oc = this.treeComponent.getControllerByNodeId(nodeId);
            oc.startRenaming();
        }, 50)
    }

    handleRenamed(e: any) {
        let newValue = e.newValue ? e.newValue : 'Kolekcja ' + (++this.noFolders);
        let parentId = this.selectedNode ? this.selectedNode : -1;
        this.saveFolder(parentId, newValue);
    }

    saveFolder(parentFolderId, folderName) {
        this.menuCollectionsService
            .addFolder(parentFolderId, folderName)
            .subscribe(
                ret => {
                    this.loadFolders();
                    this.configService.error = 'Dodano folder.';
                    this.oopNodeController = null;
                    this.selectedNode = null;
                },
                error => {
                    this.configService.error = 'Coś poszło nie tak.';
                    this.loadFolders();
                }
            );
    }

    addScanToCollection() {
        if (this.oopNodeController) {
            this.menuCollectionsService
                .addCollection(this.photosService.detail.id, this.oopNodeController.tree.id)
                .subscribe(
                    ret => {
                        this.configService.error = 'Skan został dodany do folderu.';
                    },
                    error => {
                        this.configService.error = this.configService.failMessage;
                    }
                );
        }
        else {
            this.configService.error = 'Wybierz folder';
        }
    }
}
