import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from './rest';
import { ConfigService } from './config';
import { FoldersModel } from '../models/photos';
import { ResponseModel, TabModel } from '../models/general';

@Injectable({
  providedIn: 'root',
})

export class MenuGeneralService {

    constructor(private restService: RestService, private configService: ConfigService) {
    }

    getFileXml(skanId: number): Observable<string> {
        return this.restService.get(`pobierzplikixml/${skanId}`);
    }

    sendMessage(skanId: number, body: string): Observable<string> {
        return this.restService.post(`zglosblad/${this.configService.input.user}/${skanId}`, body);
    }

    getTabs(): Observable<TabModel[]> {
        return this.restService.get(`zakladki/${this.configService.input.user}`);
    }

    addToTab(skanId: number, body: string): Observable<ResponseModel> {
        return this.restService.post(`dodajdozakladek/${this.configService.input.user}/${skanId}`, body, 'text/plain');
    }

    removeTab(idZakladki: number): Observable<ResponseModel> {
        return this.restService.delete(`usunzakladke/${this.configService.input.user}/${idZakladki}`);
    }

    /*TODO: czy wystarczy objektId, czy jeszcze userId albo cos innego mam przekazac?*/
    getTags(): Observable<String[]> {
      return this.restService.get(`pobierztagi/${this.configService.input.objektid}`);
    }

    getIndexes(): Observable<String[]> {
      return this.restService.get(`pobierzindeksy/${this.configService.input.objektid}`);
    }
}
