import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from './rest';
import { ConfigService } from './config';
import { FoldersModel } from '../models/photos';

@Injectable({
  providedIn: 'root',
})

export class MenuCollectionsService {

    constructor(private restService: RestService, private configService: ConfigService) {
    }

    addCollection(skanId: number, collectionId: number): Observable<string> {
        return this.restService.post(`dodajdokolekcji/${this.configService.input.user}/${skanId}/${collectionId}`);
    }

    getFolders(): Observable<FoldersModel[]>  {
        return this.restService.get(`katalogiuzytkownika/${this.configService.input.user}`);
    }

    addFolder(idFolderuNad: number, body: string): Observable<any>  {
        return this.restService.post(`dodajfolder/${this.configService.input.user}/${idFolderuNad}`, body, 'text/plain');
    }

     // addFolder(idFolderuNad: number, body: string): Observable<string>  {
     //     return this.restService.post(`dodajfolderPP/${this.configService.input.user}/${idFolderuNad}`, body, 'application/json');
     // }
}
