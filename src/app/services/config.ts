import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})

export class ConfigService {

    public apiServer: string = environment.apiUrl;

    public input = {
        user: null,
        jednostkaid: null,
        kolekcjaid: null,
        plikid: null,
        objektid: null,
        jezyk: null,
        idjednostkipliku: null
    };

    public message: string;
    public failMessage: string = 'Coś poszło nie tak.'
    public isLogged: boolean = false;

    constructor() {
    }

    set error(msg: string) {
        this.message = msg;
        setTimeout( () => {
            this.message = '';
        }, 1200)
    }
}
