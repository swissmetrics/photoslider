import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ConfigService } from './config';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})

export class RestService {

    constructor(private http: HttpClient, private configService: ConfigService) {
    }

    // GET method
    get(service: string): Observable<any> {
        return this.http
            .get(`${this.configService.apiServer}${service}`)
            .pipe(
              retry(1),
              // catchError(this.handleError)
            );
    }

    // POST method
    post(service: string, body: string = '', type: string = 'application/x-www-form-urlencoded'): Observable<any> {

        const httpOptions = {
            headers: new HttpHeaders({
               'Content-Type': type
            })
        };

        return this.http
            .post(`${this.configService.apiServer}${service}`, body, httpOptions)
            .pipe(
                retry(1),
                // catchError(this.handleError)
            );

    }

    // DELETE method
    delete(service: string): Observable<any> {
        return this.http
            .delete(`${this.configService.apiServer}${service}`)
            .pipe(
              retry(1),
              // catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        // console.error(error)
        return throwError('Something bad happened; please try again later.');
    };

}
