import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from './rest';
import { ConfigService } from './config';
import {PinModel, CutModel, CutEditModel} from '../models/selected';
import { ResponseModel } from '../models/general';

@Injectable({
  providedIn: 'root',
})

export class MenuSelectService {

    constructor(private restService: RestService, private configService: ConfigService) {
    }

    getPins(skanId: number): Observable<PinModel[]> {
        return this.restService.get(`pinezkipliku/${this.configService.input.user}/${skanId}/`);
    }

    addToPin(skanId: number, left: number, top: number, body: string): Observable<string> {
        return this.restService.post(`dodajdopinezek/${this.configService.input.user}/${skanId}/${left}/${top}`, body, 'text/plain');
    }

    removePin(idPinezki: number): Observable<ResponseModel> {
        return this.restService.delete(`usunpinezke/${this.configService.input.user}/${idPinezki}`);
    }

    editPin(idPinezki: number, body: string): Observable<ResponseModel> {
        return this.restService.post(`zmienopispinezki/${this.configService.input.user}/${idPinezki}`, body, 'text/plain');
    }

    saveSelect(skanId: number, left: number, top: number, width: number, height: number): Observable<string> {
        return this.restService.post(`dodajzaznaczenie/${this.configService.input.user}/${skanId}/${left}/${top}/${width}/${height}`);
    }

    getSelect(skanId: number) {
        return this.restService.get(`zaznaczeniapliku/${this.configService.input.user}/${skanId}`);
    }

    getCuts(skanId: number): Observable<CutModel[]> {
        return this.restService.get(`wycieciapliku/${this.configService.input.user}/${skanId}/`);
    }

    getCut(idWyciecia: number): Observable<CutEditModel> {
        return this.restService.get(`sprawdzwyciecia/${idWyciecia}/`);
    }

    saveCut(skanId: number, title: string, body: string): Observable<string> {
        return this.restService.post
        (`dodajwyciecie/${this.configService.input.user}/${skanId}`,
          `{"nazwa":"` + title + `", "fileInBase64":"` + body + `"}`, 'application/json');
    }

    removeCut(idWyciecie: number): Observable<string> {
        return this.restService.delete(`usunwyciecie/${this.configService.input.user}/${idWyciecie}`);
    }

    editCut(wyciecieId: number, title: string, body: string): Observable<string> {
        return this.restService.post(`zmiennazwewyciecie/${this.configService.input.user}/${wyciecieId}`, body, 'text/plain');
    }
}
