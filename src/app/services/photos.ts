import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from './rest';
import { ConfigService } from './config';
import { PhotosModel, PhotoModel, CollectionModel } from '../models/photos';
import { PinModel, CutModel } from '../models/selected';
import { ResponseModel } from '../models/general';

@Injectable({
  providedIn: 'root',
})

export class PhotosService {

    public photoOrder: number = 0;
    public zoom: number = 100;
    public zoomStep: number = 5;
    public brightness: number = 100;
    public rotate: number = 0;
    public detail: PhotoModel;
    public photoBase64: string;
    public croppedImage: string;
    public coordinates: {top: number, left: number, width: number, height: number};
    public isPin: boolean = false;
    public showSelectedPin: boolean = false;
    public pinCoordinates: {top: number, left: number} | null;
    public pins: PinModel[] | null;
    public cuts: CutModel[] | null;
    public showAllPins = false;

    constructor(private restService: RestService, private configService: ConfigService) {
    }

    imageUrl(src: string) {
        src = src.replace('localhost:8080', 'nac-dev.false.pl');
        return `url(${src})`;
    }

    set setZoom(size: number) {
        let min = 10;
        let max = 500;
        this.zoom += size;
        if (this.zoom < min) {
            this.zoom = min;
        }
        else if (this.zoom > max) {
            this.zoom = max;
        }
    }

    login(): Observable<ResponseModel> {
        return this.restService.get(`statusuzytkownika/${this.configService.input.user}`);
    }

    // get all items (photos, videos, audios)
    getItems(id: number): Observable<PhotosModel> {
        return this.restService.get('jednostka/' + id);
    }

    // get all items (photos, videos, audios)
    getItem(itemId: number): Observable<PhotoModel> {
        return this.restService.get('plik/' + itemId);
    }

    getCollections(id: number): Observable<CollectionModel> {
        return this.restService.get('kolekcja/' + id);
    }

    addToBasket(skanId: number): Observable<string> {
        return this.restService.post(`dodajdokoszyka/${this.configService.input.user}/${skanId}`);
    }

}
