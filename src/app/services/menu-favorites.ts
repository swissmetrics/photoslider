import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from './rest';
import { ConfigService } from './config';
import { FavoritesModel } from '../models/favorites';
import { ResponseModel } from '../models/general';

@Injectable({
  providedIn: 'root',
})

export class MenuFavoritesService {

    public userFavorites: FavoritesModel[] = [];

    constructor(private restService: RestService, private configService: ConfigService) {
    }

    getAllFavorites(): Observable<FavoritesModel[]> {
        return this.restService.get(`ulubione/${this.configService.input.user}`);
    }

    addFavorites(skanId: number): Observable<ResponseModel> {
        return this.restService.post(`dodajdoulubionych/${this.configService.input.user}/${skanId}`);
    }

    removeFavorites(idUlubione: number): Observable<ResponseModel> {
        return this.restService.delete(`usunulubione/${this.configService.input.user}/${idUlubione}`);
    }

}
