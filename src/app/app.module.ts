import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
import { TreeModule } from 'ng2-tree';

import { AppComponent } from './components/app';
import { TopComponent } from './components/top';
import { FooterComponent } from './components/footer';
import { DetailComponent } from './components/detail';
import { VideoComponent } from './components/video';
import { AudioComponent } from './components/audio';
import { MenuComponent } from './components/menu';
import { MenuInfoComponent } from './components/menu-info';
import { MenuSelectComponent } from './components/menu-select';
import { MenuTabComponent } from './components/menu-tab';
import { MenuCollectionComponent } from './components/menu-collection';
import { MenuDownloadComponent } from './components/menu-download';
import { MenuFavoritiesComponent } from './components/menu-favorities';
import { MenuShareComponent } from './components/menu-share';
import { DialogPhotoComponent } from './components/dialog-photo';
import { LoginComponent } from './components/login';
import { ErrorsComponent } from './components/errors';
import { CarouselComponent, CarouselItemElement } from './components/carousel';
import { CarouselItemDirective } from './directives/carousel-item';
import { MenuTagsComponent} from './components/menu-tags';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {MenuLinkComponent} from './components/menu-link';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent, TopComponent, FooterComponent, DetailComponent, DialogPhotoComponent, LoginComponent,
    MenuComponent, MenuInfoComponent, MenuSelectComponent, MenuCollectionComponent, MenuDownloadComponent,
    MenuTabComponent, MenuFavoritiesComponent, MenuShareComponent, VideoComponent, AudioComponent,
    CarouselComponent, ErrorsComponent, CarouselItemElement, CarouselItemDirective, MenuTagsComponent,
    MenuLinkComponent
  ],
  imports: [
    RouterModule.forRoot([{ path: '**', redirectTo: '' }]),
    BrowserModule, BrowserAnimationsModule, HttpClientModule, FormsModule, ImageCropperModule, TreeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
